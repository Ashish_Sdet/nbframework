package actionHelper;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;

import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import reportManager.ExtentManager;

import java.time.Duration;
import java.util.List;

public class AndroidHelperMethods {
    private AndroidDriver driver;

    public AndroidHelperMethods(AndroidDriver driver) {
        this.driver = driver;
    }

    public void click(MobileElement element) {
        WaitTillVisible(element);
        element.click();
        ExtentManager.getTest().get().info("Element is getting clicked :: Locator =" + element);
    }

    public void enter(MobileElement element, String input) {
        element.clear();
        element.sendKeys(input);
        ExtentManager.getTest().get().info("Element is getting entered :: Locator = " + input);

    }

    public void enter_DeleteLastChar(MobileElement element, String input) {
        element.clear();
        element.sendKeys(input);
        driver.pressKeyCode(67);
        ExtentManager.getTest().get().info("Element is getting entered :: Locator =" + input);
    }

    public void pressEsc() {
        try {
            Thread.sleep(2000);
            driver.pressKeyCode(111);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void WaitTillVisible(MobileElement element) {
        new WebDriverWait(driver, 15).until(ExpectedConditions.visibilityOf(element));
    }

    public void WaitTillVisible(List<MobileElement> element) {
        new WebDriverWait(driver, 15).until(ExpectedConditions.visibilityOf(element.get(0)));
    }


    public void scrollPageDownSlowly() {
        try {
            Dimension size = driver.manage().window().getSize();
            int startx = size.width / 2;
            int starty = (int) (size.height * 0.80);
            int endy = (int) (size.height * 0.35);
            System.out.println("startx = " + startx + " ,starty = " + starty + " , endy = " + endy);
            TouchAction action = new TouchAction(driver);
            action.press(startx, starty).waitAction(Duration.ofSeconds(2)).moveTo(startx, endy + 1).release().perform();
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void scrollPageDownFast() {
        try {
            Dimension size = driver.manage().window().getSize();
            int startx = size.width / 2;
            int starty = (int) (size.height * 0.70);
            int endy = (int) (size.height * 0.20);
            System.out.println("startx = " + startx + " ,starty = " + starty + " , endy = " + endy);
            TouchAction action = new TouchAction(driver);
            action.press(startx, starty).waitAction(Duration.ofSeconds(1)).moveTo(startx, endy + 150).release().perform();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public boolean isElementPresent(List<MobileElement> elements) {
        boolean flag = false;
        //  implicitWait(1);
        if (elements.size() > 0) {
            if (elements.get(0).isDisplayed()) {
                flag = true;
            }
        }
        //  implicitWait(50);
        return flag;
    }



    public void scrollToBottom(List<MobileElement> elements) {
        scrollPageDownFast();
        while (!isElementPresent(elements)) {
            scrollPageDownFast();
        }
    }
}
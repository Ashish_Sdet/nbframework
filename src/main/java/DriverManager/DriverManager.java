package DriverManager;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebDriver;


public class DriverManager {
    public static AndroidDriver driver;

    public static WebDriver getDriver(DriverManagerType browserName) throws Exception {
        switch (browserName.getDriver()) {
            case "AndroidDriver":
                String propertyPath = System.getProperty("user.dir") + "//src//main//resources//android.properties";
                return driver = new AndroidDriverManager().getAndroidDriver(propertyPath);

        }
        return null;
    }

}

package TestCases;


import DriverManager.DriverManagerType;
import DriverManager.DriverManager;
import Pages.*;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import util.utility;

import java.util.Properties;

public class BaseTest {


    static WebDriver driver;
    HomePage homePage;
    BuyPage buyPage;
    PropertiesListingPage propertiesListingPage;
    Properties dataProperties;
    LoginPage loginPage;
    WrongInfoPage wrongInfoPage;

    @BeforeSuite
    public void setUp() {
        try {
            dataProperties = utility.loadProperties(System.getProperty("user.dir") + "//src//main//resources//data.properties");
            driver = DriverManager.getDriver(DriverManagerType.ANDROID);
            homePage = new HomePage((AndroidDriver) driver);
            buyPage = new BuyPage((AndroidDriver) driver);
            propertiesListingPage = new PropertiesListingPage((AndroidDriver) driver);
            loginPage = new LoginPage((AndroidDriver) driver);
            wrongInfoPage = new WrongInfoPage((AndroidDriver) driver);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @AfterSuite
    public void destroyDriver() {
        //  driver.quit();
    }
}
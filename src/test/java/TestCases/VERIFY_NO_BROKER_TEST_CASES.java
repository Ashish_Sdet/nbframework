package TestCases;

import org.testng.Assert;
import org.testng.annotations.Test;

public class VERIFY_NO_BROKER_TEST_CASES extends BaseTest {


    @Test
    public void VERIFY_NO_BROKER_TEST_CASES() throws Exception {
        homePage.allowContinue_Btn();
        homePage.buyBtn();
        homePage.search_Btn();
        buyPage.enterLocalities(dataProperties.getProperty("localities1"));
        buyPage.enterLocalities(dataProperties.getProperty("localities2"));
        buyPage.click_two_Bhk();
        buyPage.click_three_Bhk();
        buyPage.search_Property();
        propertiesListingPage.scrollAndClick();
        propertiesListingPage.clickWrongInfo();
        loginPage.enterMobile(dataProperties.getProperty("mobile"));
        loginPage.signPassword(dataProperties.getProperty("password"));
        wrongInfoPage.clickAllCheckBox();
        wrongInfoPage.clickReport();
        wrongInfoPage.selectBHKTypeDropdown();
        wrongInfoPage.clickSaveChanges();
        Assert.assertEquals(wrongInfoPage.getThankYou(),"Thank you for the feedback");


    }
}

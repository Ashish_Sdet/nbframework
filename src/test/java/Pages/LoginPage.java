package Pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class LoginPage extends BasePageClass {

    public LoginPage(AndroidDriver driver) {
        super(driver);
    }

    public static final String signup_Mobile = "com.nobroker.app:id/et_signup_phone";

    @AndroidFindBy(id = signup_Mobile)
    MobileElement signup_mobile_input;

    @AndroidFindBy(id = "com.nobroker.app:id/rb_signup_pwd")
    MobileElement sign_password_radio_btn;

    @AndroidFindBy(id = "com.nobroker.app:id/et_signup_pwd")
    MobileElement sign_password_input;

    @AndroidFindBy(id = "com.nobroker.app:id/btn_signup")
    MobileElement continueBtn;



    public void enterMobile(String mobile){
        androidHelperMethods.enter(signup_mobile_input,mobile);
        clickSignPassRadioBtn();
    }

    public void clickSignPassRadioBtn(){
        androidHelperMethods.pressEsc();
        androidHelperMethods.click(sign_password_radio_btn);
    }
    public void signPassword(String password){
        androidHelperMethods.enter(sign_password_input,password);
        androidHelperMethods.click(continueBtn);
    }
}

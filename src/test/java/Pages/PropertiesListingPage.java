package Pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PropertiesListingPage extends BasePageClass {
    public PropertiesListingPage(AndroidDriver driver) {
        super(driver);
    }

    public static final String properties = "//android.widget.ImageView[@resource-id='com.nobroker.app:id/property_thumbnail']";
    public static final String properties_title = "//android.widget.TextView[@resource-id='com.nobroker.app:id/sub_title']";
    public static final String wrong_info = "com.nobroker.app:id/tv_report_wrong_info";
    public static final String basic_info = "com.nobroker.app:id/basic_info_title";


    @AndroidFindBy(xpath = properties)
    List<MobileElement> propertiesImageList;

    @AndroidFindBy(xpath = properties_title)
    List<MobileElement> properties_title_List;

    @AndroidFindBy(id = wrong_info)
    List<MobileElement> wrong_info_Link;

    @AndroidFindBy(id = wrong_info)
    MobileElement wrong_info_Btn;

    @AndroidFindBy(id = basic_info)
    MobileElement basic_information;


    public void scrollAndClick() {
        HashMap<String, MobileElement> prpertiesHashMap = new HashMap<>();
        androidHelperMethods.WaitTillVisible(propertiesImageList.get(0));
        while (prpertiesHashMap.size() < 4) {
            properties_title_List = androidDriver.findElements(By.xpath(properties_title));
            for (MobileElement element : properties_title_List) {
                prpertiesHashMap.put(element.getText(), element);
            }
            androidHelperMethods.scrollPageDownSlowly();
        }
        androidHelperMethods.click(properties_title_List.get(0));
    }

    public void clickWrongInfo() {
        try {
            Thread.sleep(3000);
            androidHelperMethods.scrollToBottom(wrong_info_Link);
            androidHelperMethods.click(wrong_info_Btn);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}


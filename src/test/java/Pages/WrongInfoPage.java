package Pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;

import java.util.List;

public class WrongInfoPage extends BasePageClass {
    public WrongInfoPage(AndroidDriver driver) {
        super(driver);
    }

    @AndroidFindBy(className = "android.widget.CheckBox")
    List<MobileElement> checkBox;

    @AndroidFindBy(id = "com.nobroker.app:id/btn_report")
    MobileElement report_btn;

    @AndroidFindBy(id = "com.nobroker.app:id/sp_bhk_type")
    MobileElement bhkType_dropdown;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='4+ BHK']")
    MobileElement change_BhkType;

    @AndroidFindBy(id = "com.nobroker.app:id/btn_save")
    MobileElement saveBtn;

    @AndroidFindBy(id = "com.nobroker.app:id/edt_others")
    List<MobileElement> enterNotes;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Thank you for the feedback']")
    MobileElement thankyouNote;

    public void clickAllCheckBox() {
        androidHelperMethods.WaitTillVisible(checkBox);
        checkBox.stream().forEach(element -> element.click());

    }

    public void clickReport() {
        androidHelperMethods.click(report_btn);
    }

    public void selectBHKTypeDropdown() {
        androidHelperMethods.click(bhkType_dropdown);
        androidHelperMethods.click(change_BhkType);
    }
    public void clickSaveChanges() {
        androidHelperMethods.scrollToBottom(enterNotes);
        androidHelperMethods.enter(enterNotes.get(0),"Test");
        androidHelperMethods.click(saveBtn);
    }
    public String getThankYou(){
        return thankyouNote.getText();
    }
}
